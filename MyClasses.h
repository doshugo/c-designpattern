#ifndef CAP
#define CAP
#include <iostream>
#include <stdlib.h>
#include <vector>

using namespace std;

//type �num�ratifs
enum  Mode { Route,Rail,Air }; //Mode de transport
enum  TypeGare {Ferroviaire,Aeroport}; //Type de gare

//structures

typedef struct Sstruct
{
    float valeur;
    int indice;
} TypeDual;

typedef struct Sstruct1
{
    bool existe;
    float valeur;
    int mode;
} TypeMin;

typedef struct Sstruct2
{
    int ville;
    float valeur;
    int mode;
} Ant;

typedef struct Sstruct3
{
    bool existe;
    float valeur;
} CaseTab;

//classes
template <typename nature> //objet matrice de base
class MatriceAdjacence
{
protected:
    vector <vector <nature > > matrice;

public:
    MatriceAdjacence<nature>();
    MatriceAdjacence(vector<vector <nature> > matrice);
    vector <vector<nature> > getMatrice();
};


class Gare //repr�sente une gare
{
friend ostream& operator<<( ostream &flux, Gare const& gare );

private :
    string nom; //nom de la gare
    TypeGare type; //type de gare
    Gare(string nom, TypeGare type); //constructeur

public:
    static Gare * createObject(); //cr�e un pointeur de Gare (constructeur pointeur)
    static Gare * createObjectMan(string nom, TypeGare type); //idem
    //getters
    TypeGare getType();
    string getNom();
    string toString();
    void afficher(ostream &flux) const;
};

class Ville //repr�sente Ville
{
friend ostream& operator<<( ostream &flux, Ville const& ville );

private:
    string nom; //nom de la ville
    vector < Gare * > gares; //Liste des gares et a�roports de la ville
    Ville(string nom,vector < Gare * > gares); //constucteur priv�s

public:
    //constructeur pointeur
    static Ville * createObject();
    static Ville * createObjectMan(string nom,vector < Gare * > gares);
    //destructeur
    ~Ville();
    //getters
    string getNom();
    vector < Gare * > getGares();
    bool haveFerroviaireGare();
    bool haveairport();
    string toString();
    void afficher(ostream &flux) const;
};

class Connexion //repr�sente une connexion entre 2 villes
{
private :
    bool existe; //v�rifie si il existe une connexion
    float distance; //distance de la connexion
    float duree; //duree de la connexion
    float prix; //prix(en euros) de la connexion

public :
    //Constructeurs
    Connexion(); //Par d�faut quand absence d'arrete, existe = false
    Connexion(float distance, float duree, float prix);
    //getters
    bool getExiste();
    float getDistance();
    float getDuree();
    float getPrix();
};

class Pays //repr�sente le pays
{
private:
    string nom; //nom du pays
    vector <Ville * > villes; //liste de villes
    vector < vector < vector < Connexion * > > > connexions;  // liste de connexions (1e indice mode , 2e sommet depart, 3e sommet arrivee)
    //constructeurs
    Pays(string nom,vector <Ville *> villes,vector < vector < vector < Connexion * > > > connexions);

public :
    //constructeur pointeur
    Pays();
    static Pays * createObjectMan(string nom,vector <Ville *> villes,vector < vector < vector < Connexion * > > > connexions);
    //constructeur connexion
    static vector < vector < vector < Connexion * > > > matriceDepart(int nbVilles);
    //getters
    string getNom();
    int getIndiceVille(string nomVille);
    vector <Ville *> getVilles();
    vector < vector < vector < Connexion * > > > getConnexions();
    //m�thodes dynamiques
    void visite(int indVille); //viste des voisin � partir des voisins d'indice indVille
    vector <int> explorer(int indVille, vector <int > villeExplorees,int degre); //explore un pays � partir d'une ville
    void meilleurCheminCritere(int v1,int v2,int m,int c);
    vector < vector <int> > dijkstra(int v1,int v2,int m, int c);
    vector <Ville *> getVillesFerroviaire();
    vector <Ville *> getVillesAeroportuaire();
};

/*
class Transportation
{
public :
    virtual void meilleurChemin(Pays pays, int v1, int v2) = 0;
};

class DefaultTransport : public Transportation
{
public :
    DefaultTransport();
    void meilleurChemin(Pays pays, int v1, int v2);
};

class TransportCritereCout : public Transportation
{
private :
    int critere;
public :
    TransportCritereCout(int critere);
    void meilleurChemin(Pays pays, int v1, int v2);
};  */
class MatriceReel : public MatriceAdjacence<float>
{
public :
    void setMatrice(vector<float> vect);
    void set_in_Matrice(int i, int j, float value);
    void afficher();

};


class AdaptePays : public MatriceAdjacence<TypeMin> //Adapte le pays en un graphe (matrice d'adjacence)
{

private :
    vector <Ville*> villes; //liste de ville (pour le nom des villes)

public :
    //constructeurs
    AdaptePays(Pays * pays,int c);
    //getters
    vector <Ville*> getVilles();
    MatriceReel* getMatriceValues();
    //m�thode dynamique
    void afficherMatrice(); //affiche la matrice d'ajacence
    void meilleurCheminCritereToutMode(int v1,int v2,int c);
    void meilleurCheminEscale(int v1, int v2);
    vector<int> findCheminEscale(int v1, int v2, int n);
    float evaluationChemin(vector <int> chemin);
    int nbEscale(int v1,int v2);
    vector < vector <int> > dijkstraToutMode(int v1,int v2);
    void changeCritere(Pays * pays, int c);
};


class Prog   //proc�dure pour le main (adapt� un singleton pour la classe)
{
private :
    static int* existe;
    Prog();

public :
    static void lancement();
    static Prog getProg();
    static  Pays * remplirPays();
    static  Pays * remplirPaysMan();
    static  Pays * chargerPaysXml();
};

class Facade
{
    private:
        Pays * pays;
        AdaptePays * adapte;

    public:
        Facade();
        Facade(Pays * pays);
        ~Facade();
        void chargerGraphe();
        void chercherVoisin();
        void chercherVoisin(string nomVille);
        void afficherGraphe();
        void afficherGraphe(string critere);
        void afficherVillesbyTransportation();
        void afficherVillesbyTransportation(string critere);
        void meilleurChemin();
        void meilleurChemin(string ville1, string ville2, string critere);
        void meilleurCheminTousTransport();
        void meilleurCheminTousTransport(string ville1, string ville2, string critere);
        Pays * getPays();
        AdaptePays * getAdaptePays();
};

//op�rateur
ostream& operator<<( ostream &flux, Gare const& gare );
ostream& operator<<( ostream &flux, Ville const& ville );

//fonction hors-classes
float minimum(vector <float> liste); //minimum
TypeMin  minimum(vector < vector < vector < Connexion*> > > connexion, int idVille1, int idVille2,int critere); //minimum de critere (distance, ou cout ou prix) entre deux villes
TypeDual minimumPos(vector <float> liste); //minimum en 1er indice et position en 2eme
float arrondir(float valeur, int echelle); //renvoie valeur � �celle chiffre apr�s la virgule pr�s
float puissance(float valeur, float res, int exp); //renvoie la valeur d'exposant exp.
float multiplication_ligne(MatriceReel* M, MatriceReel* N, int i,int j);
MatriceReel* produit_matriciel(MatriceReel* M,MatriceReel* N);
MatriceReel* puissance(MatriceReel* M, int n);//on va une fonction puissance et suppos� que n > 0 tout le temps



#endif // CAP
