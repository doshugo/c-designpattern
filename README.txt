Calcul d'itinéraire optimal en C++ :

Ce programme prend en entrée un graphe au format xml, deux points du graphe et renvoie
un itinéraire optimal selon certains critères choisis.

L'implémentation de ce code répond aux critères du livrable 1.

Compiler et exécuter les 3 fichiers, affichera automatiquement les 2 modes d'affichage implémenté (détaillé et matrice).

Si vous voulez voir comment le graphe est chargé, il faudra voir l'implémentation méthodes de la classe Prog.

CONCERNANT LES CRITERES DE LA RECHERCHE DU MEILLEUR CHEMIN (menu 3) :
	Pour rentrer les critères, vous devez rentrez une chaine de caractère de la forme :
		chaine = chaine1+chaine2 (Le signe '+' signifie une concaténation sans espace des deux chaines)
	
	La chaine 1 reprsrésente le critère de transport a pour valeur :
		-route
		-train
		-avion
		
	La chaine 2 représente le critère de mesure de recherche optimal (la coût de l'arrête) pour valeur
		-1 pour la distance
		-2 pour la durée
		-3 pour le prix
		
	Une de ces chaines ou ces deux chaines peuvent être vide dans ce cas la recherche se fait par défaut (meilleur chemin par la route suivant le prix du chemin)
	
	Ex (les guillemets ne sont pas à rentré en paramètre) :
		x chaine = ""  ERREUR
		x chaine = "1train" ERREUR
		chaine = "train1" -> recherche du meilleur chemin par le train suivant le coût en distance
		chaine = "avion" -> recherche du meilleur chemin par la voie aérienne suivant le coût en prix
		chaine = "2" -> recherche du meilleur par la route suivant le coût en durée
		
CONCERNANT LES CRITERES DU MEILLEUR CHEMIN TOUS TRANSPORTS (menu 4) :
	Les 3 seules critères à rentrer sont :
		"prix", "duree", "distance"
	Elles correspondent aux criteres de coût.