#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "MyClasses.h"
#include <vector>
#include <algorithm>
#include <sstream>
#include <cstdlib>
#include <limits>
#ifdef _WIN32
    #include <conio.h>
#else
    #include <termios.h>
    #include <unistd.h>
#endif
#include "tinyxml.h"
#define MAXIMUM 100000000
using namespace std;

//fonction conversion LINUX WINDOWS
#ifdef _WIN32

#else
void mode_raw(int activer)
{
    static struct termios cooked;
    static int raw_actif = 0;

    if (raw_actif == activer)
        return;

    if (activer)
    {
        struct termios raw;

        tcgetattr(STDIN_FILENO, &cooked);

        raw = cooked;
        cfmakeraw(&raw);
        tcsetattr(STDIN_FILENO, TCSANOW, &raw);
    }
    else
        tcsetattr(STDIN_FILENO, TCSANOW, &cooked);

    raw_actif = activer;
}
#endif // _unix_

void clearscreen()
{
    #ifdef _WIN32
        system("CLS");
    #else
        system("clear");
    #endif // _WIN32
}

void getTouch()
{
    #ifdef _WIN32
        _getch();
    #else
    char c;
    fflush(stdin);
       mode_raw(1);
    c=getchar();
    c=getchar();
    mode_raw(0);
    #endif // _unix_
}
/****************/
//fonction erreur
void vider_buffer()
{
    cin.clear();
    cin.seekg(0, ios::end);

    if(!cin.fail())
    {
        cin.ignore(numeric_limits<streamsize>::max()); // Le flux a déjà un état valide donc inutile de faire appel à clear()
    }

    else
    {
        cin.clear(); // Le flux est dasn un état invalide donc on le remet en état valide
    }
}

/****************/

//opérateur
ostream& operator<<( ostream &flux, Gare const& gare )
{
    gare.afficher(flux);
    return flux;
}

ostream& operator<<( ostream &flux, Ville const& ville )
{
    ville.afficher(flux);
    return flux;
}

/****************/

//fonctions de base
float minimum(vector <float> liste)
{
    if((int) liste.size() == 0)
        return 0;
    else
    {
        int res = 99999;
        for(int i = 0; i < (int) liste.size(); i++)
        {
            if(res > liste[i])
                res = liste[i];
        }
        return res;
    }
}


TypeDual minimumPos(vector <float> liste)
{
    TypeDual m;
    if((int) liste.size() > 0)
    {
        m.valeur=minimum(liste);
        int i = 0;
        while(i <(int) liste.size() && liste[i]!=m.valeur)
            i--;
        m.indice=i;
    }
    return m;
}

TypeMin minimum(vector < vector < vector < Connexion*> > > connexion, int idVille1, int idVille2,int critere) //critere: 0 distance, 1 duree, 2 prix
{
    vector <float> liste;
    vector <TypeDual> liste2;
    float n1;
    int cpt=0;
    TypeMin mi;
    mi.existe=false;
    mi.valeur = 0;
    mi.mode = 666;
    for(int i = 0; i < 3; i++)
    {
        if(connexion[i][idVille1][idVille2]->getExiste() == true)
        {
            TypeDual m;
            switch (critere)
            {
                case 0:
                    n1=connexion[i][idVille1][idVille2]->getDistance();
                    break;
                case 1:
                    n1=connexion[i][idVille1][idVille2]->getDuree();
                    break;
                case 2:
                    n1=connexion[i][idVille1][idVille2]->getPrix();
                    break;
            }
            liste.push_back(n1);
            m.indice=i;
            m.valeur=n1;
            liste2.push_back(m);
            cpt=cpt+1;
        }
    }
    if(cpt>0)
    {
        TypeDual t= minimumPos(liste);
        mi.existe=true;
        mi.valeur=liste2[t.indice].valeur;
        mi.mode=liste2[t.indice].indice;
    }
    return mi;
}

float puissance(float valeur, float res, int exp)
{
    switch(exp)
    {
        case 0 : return 1;
        case 1 : return res;
        default : return puissance(valeur, res*valeur, exp-1);
    }
}

float arrondir(float valeur, int echelle)
{
    float decimal = valeur * puissance(10, 1, echelle+1);
    float reste = decimal - (int) decimal;
    if(reste > 0.5)  return ((int) decimal+1)/puissance(10,1,echelle+1);
    else return ((int) decimal)/puissance(10,1,echelle+1);
}

float multiplication_ligne(MatriceReel* M, MatriceReel* N, int i,int j)
{
    float res = 0;
    for(int k=0;k < (int)M->getMatrice().size();k++)
        res += M->getMatrice()[k][j] * N->getMatrice()[i][k];
    return res;
}

MatriceReel* produit_matriciel(MatriceReel* M,MatriceReel* N)
{
MatriceReel* L = M;
for(int i=0;i<(int)L->getMatrice()[0].size();i++)
    for(int j=0;j<(int)L->getMatrice()[0].size();j++)
        L->set_in_Matrice(i,j,multiplication_ligne(M,N,i,j)); //opération ligne x colonne
return L;
}

MatriceReel* puissance(MatriceReel* M, int n) //on va une fonction puissance et supposé que n > 0 tout le temps
{
    if(n == 0 || n == 1)
        return M;
    else
        return produit_matriciel(M,puissance(M,n-1));
}

/**MatriceAdjacence ***/
template <typename nature>
vector <vector <nature> > MatriceAdjacence<nature>::getMatrice()
{
    return this->matrice;
}

template <typename nature>
MatriceAdjacence<nature>::MatriceAdjacence()
{

}

template <typename nature>
MatriceAdjacence<nature>::MatriceAdjacence(vector <vector <nature> > matrice)
{
    this->matrice=matrice;
}
/************/

/************ Gare ************/
Gare * Gare::createObject()
{
    int c;
    TypeGare type;
    string nom;
    Gare * gare;
    cout << "\nEntrer le num du choix :" << endl;
    cout << "\n1- Gare aerienne :" << endl;
    cout << "\n2-Gare Ferrovaire :" << endl;
    cin>>c;
    if (c==1)
        type = Aeroport;
    else
        type = Ferroviaire;
    cout << "Entrez le nom de la gare" << endl;
    cin.clear();
    cin>>nom;
    gare= new Gare(nom,type);
    return gare;
}

Gare * Gare::createObjectMan(string nom, TypeGare type)
{
    Gare * gare = new Gare (nom,type);
    return gare;
}


Gare::Gare(string nom,TypeGare type)
{
    this->nom=nom;
    this->type=type;
}

TypeGare Gare::getType()
{
    return this->type;
}

string Gare::getNom()
{
    return this->nom;
}

string Gare::toString()
{
    string enonce = "Nom : " + this->nom + ", type : ";
    if(this->type == Ferroviaire)
        enonce += "gare";
    else
        enonce += "aeroport";
    return enonce;
}

void Gare::afficher(ostream &flux) const
{
    string enonce = "Nom : " + this->nom + ", type : ";
    if(this->type == Ferroviaire)
        enonce += "gare";
    else
        enonce += "aeroport";
    flux << enonce;
}
/***************/

/****** DefaultTransport *****/



/****************/

/***** Ville **********/
Ville *  Ville::createObject()
{
    string nom;
    vector <Gare*> gares;
    int n;
    cout << "\n Entrez le nom de la ville" << endl;
    cin >> nom;
    cout << "\n Entrez le nombre de gares" << endl;
    cin >> n;
    for (int i=0; i<n; i++)
    {
        cout << "\n Gare n°" << (i+1)<< endl;
        Gare * gare = Gare::createObject();
        gares.push_back(gare);
    }
    Ville * ville = new Ville (nom,gares);
    return ville;
}

Ville * Ville::createObjectMan(string nom,vector < Gare *> gares)
{
    Ville * ville = new Ville (nom,gares);
    return ville;
}

Ville::Ville(string nom,vector < Gare *> gares)
{
    this->nom=nom;
    this->gares=gares;
}

string Ville::getNom()
{
    return this->nom;
}

bool Ville::haveairport()
{
    int i = 0;
    while(i < (int) this->getGares().size() && (*this->getGares()[i]).getType() != Aeroport)
            i++;
    if(i < (int) this->getGares().size()) return true;
        else return false;
}

bool Ville::haveFerroviaireGare()
{
    int i = 0;
    while(i < (int) this->getGares().size() && (*this->getGares()[i]).getType() != Ferroviaire)
            i++;
    if(i < (int) this->getGares().size()) return true;
        else return false;
}

vector < Gare *> Ville::getGares()
{
    return this->gares;
}

string Ville::toString()
{
    string affiche = "Ville : "+this->nom + "\n ";
    if((int) this->gares.size() > 0)
    {
        affiche += "   Gares :\n";
        for(int i=0; i< (int) gares.size();i++)
            affiche+= "        "+gares[i]->toString()+"\n \n";
    }
    return affiche;
}

void Ville::afficher(ostream &flux) const
{
   string affiche = "Ville : "+this->nom + "\n ";
    if((int) this->gares.size() > 0)
    {
        affiche += "\n   Gares :\n";
        for(int i=0; i< (int) gares.size();i++)
            affiche+= "        "+gares[i]->toString()+"\n";
    }
    flux<<affiche;
}
/****************/

/**** Connexion ****/

Connexion::Connexion()
{
    this->existe=false;
}

Connexion::Connexion(float distance, float duree, float prix)
{
    this->existe=true;
    this->distance=distance;
    this->duree=duree;
    this->prix=prix;
}

bool Connexion::getExiste()
{
    return this->existe;
}

float Connexion::getDistance()
{
    return this->distance;
}

float Connexion::getDuree()
{
    return this->duree;
}

float Connexion::getPrix()
{
    return this->prix;
}

/*******/

/**** Pays ****/
Pays::Pays()
{

}

Pays::Pays(string nom,vector <Ville*> villes,vector < vector < vector < Connexion* > > > connexions)
{
    this->nom=nom;
    this->villes=villes;
    this->connexions=connexions;
}

Pays * Pays::createObjectMan(string nom,vector <Ville *> villes,vector < vector < vector < Connexion * > > > connexions)
{
    Pays * pays = new Pays (nom,villes,connexions);
    return pays;
}

string Pays::getNom()
{
    return this->nom;
}

int Pays::getIndiceVille(string nomVille)
{
    int i = 0;
    while((i < (int) this->getVilles().size()) && (this->getVilles()[i]->getNom() != nomVille))
        i++;
    return i;
}

vector <Ville*> Pays::getVilles()
{
    return this->villes;
}

vector<Ville *> Pays::getVillesAeroportuaire()
{
    vector <Ville *> villes;
    for(int i=0; i< (int) this->getVilles().size();i++)
        if(this->getVilles()[i]->haveairport())
            villes.push_back(this->getVilles()[i]);
    return villes;
}

vector<Ville *> Pays::getVillesFerroviaire()
{
    vector <Ville *> villes;
    for(int i=0; i< (int) this->getVilles().size();i++)
        if(this->getVilles()[i]->haveFerroviaireGare())
            villes.push_back(this->getVilles()[i]);
    cout << this->getVilles()[0]->haveFerroviaireGare() << endl;
    return villes;
}

vector < vector < vector < Connexion*> > > Pays::getConnexions()
{
    return this->connexions;
}

vector < vector < vector < Connexion * > > > Pays:: matriceDepart(int nbVilles)
{
    vector < vector < vector < Connexion * > > > mat;
    int i,j,k;
    for(i=0; i<3; i++)
    {
        mat.push_back(vector < vector < Connexion * > >());
        for(j=0; j<nbVilles; j++)
        {
            mat[i].push_back( vector < Connexion * > ());
            for(k=0; k<nbVilles; k++)
            {
                mat[i][j].push_back(new Connexion());
            }
        }
    }
    return mat;
}

void Pays::visite(int indVille)
{
    vector <int> villeExplorees;
    explorer(indVille,villeExplorees,this->getVilles().size());
}


vector <int>  Pays::explorer(int indVille, vector <int > villeExplorees, int degre)
{
    vector <int > voisins;
    cout << *(villes[indVille])<< endl <<endl;
    cout <<"   Ville(s) voisine(s) :"<<endl;
    //liste villes voisines
    for (int i=0; i<(int) villes.size(); i++)
    {
        if ((connexions[0][indVille][i]->getExiste()==true)||(connexions[1][indVille][i]->getExiste()==true)||(connexions[2][indVille][i]->getExiste()==true))
        {
            voisins.push_back(i);
            cout << "\n     La ville :" << villes[i]->getNom()<<endl;
            cout << "\n     Modes de transport disponibles pour y aller:" <<endl;
            string  mode []= {"Route","Train","Avion"};
            for (int id=0; id<3; id++)
            {
                if (connexions[id][indVille][i]->getExiste()==true)
                    cout << "\n        Mode: " << mode[id] << " / Cout : " << connexions[id][indVille][i]->getPrix() << " euros / Distance : " << connexions[id][indVille][i]->getDistance() << " km / Duree : " << connexions[id][indVille][i]->getDuree() << " minutes " << endl;
            }
        }
    }
    // ajout a la liste
    villeExplorees.push_back(indVille);
    if(degre > 1 && villeExplorees.size() < this->getVilles().size())
        {
            for (int i=0; i<(int) voisins.size(); i++)
            {
                int ind = voisins[i];
                if (find(villeExplorees.begin(), villeExplorees.end(), ind) ==villeExplorees.end())
                {
                villeExplorees = explorer(ind,villeExplorees, degre-1);
                }
            }
        }
    return villeExplorees;
}

void Pays::meilleurCheminCritere(int v1,int v2,int m,int c)
{
    vector < vector <int > > chemin;
    int e=1;
    string  mo []= {"Route","Rail","Air"};
    cout << "Mode : " <<mo[m] << endl <<endl;
    double sum=0;
    string s;
    float coutConnexion;
    chemin = dijkstra(v1,v2,m,c);
    if(chemin.size()>0)
    {
        cout << "Chemin optimal : " <<endl <<endl;
        for (int i=chemin.size()-1; i>=0; i--)
        {
            cout << "   Etape no " <<e<<endl ;
            switch (c)
            {
                case 0:
                    coutConnexion = this->getConnexions()[m][chemin[i][1]][chemin[i][2]]->getDistance();
                    break;
                case 1:
                    coutConnexion = this->getConnexions()[m][chemin[i][1]][chemin[i][2]]->getDuree();
                    break;
                case 2:
                    coutConnexion =  this->getConnexions()[m][chemin[i][1]][chemin[i][2]]->getPrix();
                    break;
            }
                cout << "       Depart  : " <<this->getVilles()[chemin[i][1]]->getNom() << "\n       Arrivee : " <<this->getVilles()[chemin[i][2]]->getNom() << "\n       cout    : "  << coutConnexion << endl << endl;
                sum += coutConnexion;
                e++;
        }
        switch (c)
        {
            case 0 :
                s = " km";
                break;
            case 1 :
                s = " minutes";
                break;
            case 2 :
                s = " euros";
                break;
        }
        cout<< "cout total : " << sum << s  <<endl<< endl;
    }
    else
        cout << "Il n'existe pas de chemin reliant les deux villes " <<endl <<endl;
}

vector < vector <int> > Pays::dijkstra(int v1,int v2,int m, int c)
{
    int taille = (int) this->getVilles().size();
    int n,ind;
    float p;
    int villeCourante=v1;
    vector<Ant> pilot;
    vector< vector <CaseTab> > tableau;
    vector < vector <int> > chem;
    Ant deb;
    deb.ville=v1;
    deb.valeur=0;
    pilot.push_back(deb);
    for(int i=0; i<taille; i++)
    {
        tableau.push_back(vector<CaseTab>());
        for(int j=0; j<taille; j++)
        {
            CaseTab t;
            t.existe=true;
            t.valeur=(float) MAXIMUM;
            tableau[i].push_back(t);
        }
    }
    for(int i=0; i<taille; i++)
    {
        for (int k=i; k<taille; k++)
            tableau[k][villeCourante].existe=false;
        for (int j=0; j<taille; j++)
        {
            if(tableau[i][j].existe)
            {
                if(this->getConnexions()[m][villeCourante][j]->getExiste())
                {
                    p = 0;
                    switch(c)
                    {
                        case 0:
                            p = this->getConnexions()[m][villeCourante][j]->getDistance() + pilot[i].valeur;
                            break;
                        case 1:
                            p = this->getConnexions()[m][villeCourante][j]->getDuree() + pilot[i].valeur;
                            break;
                        case 2:
                            p = this->getConnexions()[m][villeCourante][j]->getPrix() + pilot[i].valeur;
                            break;
                    }
                    if(p < tableau[i][j].valeur)
                        for(int k=i; k<taille; k++)
                            tableau[k][j].valeur=p;
                }
            }
        }
        double ri=10000000000000;
        for(int k1=0; k1<taille; k1++)
        {
            if(tableau[i][k1].existe && tableau[i][k1].valeur<ri)
            {
                ri=tableau[i][k1].valeur;
                ind=k1;
            }
        }
        Ant nouv;
        nouv.ville=ind;
        nouv.valeur=ri;
        pilot.push_back(nouv);
        villeCourante=ind;
    }
    villeCourante=v2;
    while(villeCourante!=v1)
    {
        n=tableau.size()-1;
        while (!tableau[n][villeCourante].existe || (n>0 && tableau[n][villeCourante].valeur==tableau[n-1][villeCourante].valeur))
                n--;
        if(villeCourante==v2&&tableau[n][villeCourante].valeur==(float) MAXIMUM)
            break;
        vector <int > ch;
        ch.push_back(m);
        ch.push_back(pilot[n].ville);
        ch.push_back(villeCourante);
        chem.push_back(ch);
        villeCourante=pilot[n].ville;
    }
    return chem;
}
/*********/

/*** MatriceReel ***/
void MatriceReel::setMatrice(vector<float> vect)
{
    this->matrice.push_back(vect);
}

void MatriceReel::set_in_Matrice(int i, int j, float value)
{
    if(i < (int) this->matrice[0].size() && j < (int) this->matrice[0].size() )
        this->matrice[i][j] = value;
}

void MatriceReel::afficher()
{
   ostringstream oss;
    for(int i = 0; i < (int) this->getMatrice()[0].size();i++)
    {
        for(int j=0; j <(int) this->getMatrice()[0].size();j++)
        {
            oss.str("");
            oss.clear();
            oss << arrondir((this->getMatrice()[i][j]),1);
            if(oss.str().size() == 1)
                cout << "   "<< oss.str() << "   ";
                else  if(oss.str().size() == 2)
                    cout << "  "<< oss.str() << "   ";
                    else if(oss.str().size() == 3)
                        cout << "  "<< (this->getMatrice()[i][j]) << "  ";
                        else if(oss.str().size() == 4)
                            cout << " "<< oss.str() << "  ";
                                else  cout << " "<< oss.str() << " ";
        }
        cout << endl;
    }
}

/*****/

/*** AdaptePays ***/
AdaptePays::AdaptePays(Pays * pays,int c) : MatriceAdjacence<TypeMin>()
{
    for(int i = 0; i < (int) pays->getVilles().size(); i++)
    {
        this->villes.push_back(pays->getVilles()[i]);
    }
    for(int i=0; i < (int)this->getVilles().size(); i++)
    {
        this->matrice.push_back(vector <TypeMin> ());
        for(int j=0; j < (int)this->getVilles().size(); j++)
        {

            this->matrice[i].push_back((minimum(pays->getConnexions(),i,j,c)));
        }
    }
}

void AdaptePays::changeCritere(Pays * pays, int c)
{
        for(int i=0; i < (int)this->getVilles().size(); i++)
    {
        for(int j=0; j < (int)this->getVilles().size(); j++)
        {

            this->matrice[i][j] = minimum(pays->getConnexions(),i,j,c);
        }
    }
}
vector <Ville*> AdaptePays::getVilles()
{
    return this->villes;
}

MatriceReel* AdaptePays::getMatriceValues()
{
    vector <float> vect;
    MatriceReel* values = new MatriceReel();
    for(int i = 0; i < (int) this->getVilles().size();i++)
    {
        vect = vector <float> ();
        for(int j=0; j <(int) this->getVilles().size();j++)
        {
            if(this->getMatrice()[i][j].valeur > 0)
                vect.push_back(1);
            else vect.push_back(0);
        }
        values->setMatrice(vect);
    }
    return values;
}

void AdaptePays::afficherMatrice()
{
    cout << "Nombre de ville : " << (int)this->getVilles().size() <<"\nListe des villes par lignes et colonnes :  \n" << endl;
    for(int i=0; i < (int)this->getVilles().size();i++)
        cout << "   Ligne et colonne n°" << i <<" : "<< this->getVilles()[i]->getNom() <<endl;
    cout << endl;
    ostringstream oss;
    for(int i = 0; i < (int) this->getMatrice()[0].size();i++)
    {
        for(int j=0; j <(int) this->getMatrice()[0].size();j++)
        {
            oss.str("");
            oss.clear();
            oss << arrondir((this->getMatrice()[i][j].valeur),1);
            if(oss.str().size() == 1)
                cout << "   "<< oss.str() << "   ";
                else  if(oss.str().size() == 2)
                    cout << "  "<< oss.str() << "   ";
                    else if(oss.str().size() == 3)
                        cout << "  "<< (this->getMatrice()[i][j].valeur) << "  ";
                        else if(oss.str().size() == 4)
                            cout << " "<< oss.str() << "  ";
                                else  cout << " "<< oss.str() << " ";
        }
        cout << endl;
    }
}



void AdaptePays::meilleurCheminCritereToutMode(int v1,int v2,int c)
{
    vector < vector <int > > chemin;
    int e=1;
    string  mo []= {"Route","Rail","Air"};
    double sum = 0;
    chemin = dijkstraToutMode(v1,v2);
    if(chemin.size()>0)
    {
        cout << "\n Chemin optimal :" <<endl;
        for (int i=chemin.size()-1; i>=0; i--)
        {
            cout << "\n Etape no " <<e<<endl;
            switch (c)
            {
            case 0:
                cout << " Mode : " << mo[ chemin[i][0] ] <<  " Depart : " <<this->getVilles()[chemin[i][1]]->getNom() << "   Arrivee : " <<this->getVilles()[chemin[i][2]]->getNom() << "   cout : "  << this->getMatrice()[chemin[i][1]][chemin[i][2]].valeur << " km" << endl;
                break;
            case 1:
                cout <<  " Mode : " << mo[ chemin[i][0] ] <<               "  Depart : " <<this->getVilles()[chemin[i][1]]->getNom() << "   Arrivee : " <<this->getVilles()[chemin[i][2]]->getNom() << "   cout : "  << this->getMatrice()[chemin[i][1]][chemin[i][2]].valeur <<" minutes" << endl;
                break;
            case 2:
                cout <<   " Mode : " << mo[ chemin[i][0] ] <<              "  Depart : " <<this->getVilles()[chemin[i][1]]->getNom() << "   Arrivee : " <<this->getVilles()[chemin[i][2]]->getNom() << "   cout : "  << this->getMatrice()[chemin[i][1]][chemin[i][2]].valeur <<" euros" << endl;
                break;
            }
            sum += this->getMatrice()[chemin[i][1]][chemin[i][2]].valeur;
            e++;
        }
        switch (c)
        {
            case 0 :
                cout<< "\n cout total : " << sum << " km"  <<endl;
                break;
            case 1 :
                cout<< "\n cout total : " << sum << " minutes"  <<endl;
                break;
            case 2 :
                cout<< "\n cout total : " << sum << " euros"  <<endl;
                break;
        }
    }
    else
        cout << "\n Il n'existe pas de chemin reliant les deux villes " <<endl;
}

vector < vector <int> > AdaptePays::dijkstraToutMode(int v1,int v2)
{
    int taille = this->getVilles().size();
    int villeCourante=v1;
    vector<Ant> pilot;
    vector< vector <CaseTab> > tableau;
    vector < vector <int> > chem;
    int n, ind;
    Ant deb;
    deb.ville=v1;
    deb.valeur=0;
    pilot.push_back(deb);
    for(int i=0; i<taille; i++)
    {
        tableau.push_back(vector<CaseTab>());
        for(int j=0; j<taille; j++)
        {
            CaseTab t;
            t.existe=true;
            t.valeur=(float) MAXIMUM;
            tableau[i].push_back(t);
        }
    }
    for(int i=0; i<taille; i++)
    {
        for (int k=i; k<taille; k++)
            tableau[k][villeCourante].existe=false;
        for (int j=0; j<taille; j++)
        {
            if(tableau[i][j].existe)
            {
                if(this->getMatrice()[villeCourante][j].existe)
                {
                    float p;
                    p = this->getMatrice()[villeCourante][j].valeur + pilot[i].valeur;
                    if(p<tableau[i][j].valeur)
                    {
                        for(int k=i; k<taille; k++)
                            tableau[k][j].valeur=p;
                    }
                }
            }
        }
        double ri=10000000000000;
        for(int k1=0; k1<taille; k1++)
        {
            if(tableau[i][k1].existe && tableau[i][k1].valeur<ri)
            {
                ri=tableau[i][k1].valeur;
                ind=k1;
            }
        }
        Ant nouv;
        nouv.ville=ind;
        nouv.valeur=ri;
        pilot.push_back(nouv);
        villeCourante=ind;
    }
    villeCourante=v2;
    while(villeCourante!=v1)
    {
        n=tableau.size()-1;
        while (!tableau[n][villeCourante].existe || (n>0 && tableau[n][villeCourante].valeur==tableau[n-1][villeCourante].valeur))
                n--;
        if(villeCourante==v2&&tableau[n][villeCourante].valeur==(float) MAXIMUM)
        {
            break;
        }
        vector <int > ch;
        ch.push_back(this->getMatrice()[pilot[n].ville][villeCourante].mode);
        ch.push_back(pilot[n].ville);
        ch.push_back(villeCourante);
        chem.push_back(ch);
        villeCourante=pilot[n].ville;
    }
    return chem;
}

float AdaptePays::evaluationChemin(vector<int> chemin)
{
    float res = 0;
    for(int i = 1; i < (int) chemin.size();i++)
        res+= this->getMatrice()[chemin[i-1]][chemin[i]].valeur;
    return res;
}

int AdaptePays::nbEscale(int v1, int v2)
{
    int n = 1;
    while(puissance(this->getMatriceValues(),n)->getMatrice()[v1][v2] == 0 && (n < (int)this->getVilles().size() - 1))
        n++;
    return n;
}

vector<int> AdaptePays::findCheminEscale(int v1, int v2, int n)
{
    MatriceReel* matrice = this->getMatriceValues();
    vector<int> chemin, etats1, etats2;
    if(n == 0 && v1 != v2)
        n = nbEscale(v1,v2);
    if(n >= (int) this->getVilles().size())
        return vector<int>();
    else
    {
        for(int i=0; i<n+1;i++)
                chemin.push_back(0);
        chemin[0] = v1;
        chemin[n] = v2;
        if(n==1)
         return chemin;
        else
        {
            vector<vector<int> > cheminPossibles;
            vector<float> evaluations;
            int res, pos;
            pos = 0;
            for(int i = 0; i < (int) this->getVilles().size();i++)
            {
                etats1.push_back(0);
                etats2.push_back(0);
            }
            for(int i = 0; i < (int) this->getVilles().size();i++)
            {
                if(puissance(matrice,1)->getMatrice()[v1][i] > 0)
                {
                    etats1[i] = 1;
                }
                else etats1[i] = 0;
                if(puissance(matrice,n-1)->getMatrice()[i][v2] > 0)
                    etats2[i] = 1;
                else etats2[i] = 0;
                if(etats1[i] == etats2[i] && etats1[i] == 1)
                {
                    cheminPossibles.push_back(this->findCheminEscale(i,v2,n-1));
                }

            }
            cout << "res "<< n <<endl;
            for(int i = 0; i < (int)this->getVilles().size();i++)
            {
                cout << etats1[i] << " ";
            }
            cout << endl;
             for(int i = 0; i < (int)this->getVilles().size();i++)
            {
                cout << etats2[i] << " ";
            }
            cout << endl;
            for(int i = 0; i<(int) cheminPossibles.size();i++)
                evaluations.push_back(this->evaluationChemin(cheminPossibles[i]));
            res = minimum(evaluations);
            cout << res << endl;
            while(pos < (int) evaluations.size() && evaluations[pos] < res)
                pos++;
            for(int i=0; i < (int) cheminPossibles.size();i++)
                chemin[i+1] = cheminPossibles[pos][i];
            return chemin;
        }
    }

}

void AdaptePays::meilleurCheminEscale(int v1, int v2)
{
    vector<int> chemin;
    float sum = 0;
    chemin = this->findCheminEscale(v1,v2,0);
    if((int) chemin.size() > 0)
    {
        cout << "Chemin optimal :" << endl;
        for(int i = 0; i < (int) chemin.size()-1; i++)
        {
            cout << "   Etape " << i+1 << " : " << endl;
            cout << "       Depart : " << this->getVilles()[chemin[i]]->getNom() << endl;
            cout << "       Finish : " << this->getVilles()[chemin[i+1]]->getNom() << endl;
            cout << "       Cout   : " << this->getMatrice()[chemin[i]][chemin[i+1]].valeur << endl << endl;
            sum += this->getMatrice()[chemin[i]][chemin[i+1]].valeur;
        }
        cout << endl << "Cout total : "<<sum<< endl;
    }
    else
        cout << "\n Il n'existe pas de chemin reliant les deux villes " <<endl;
}

/*******/

void Prog::lancement()
{
    int cont =1;
    int init =0;
    int ch;//c,v1,v2;
    Facade* facade;

    while (cont ==1)
    {
        cout << "MENU PRINCIPAL \n" << endl;
        cout << "1: Charger le pays depuis le fichier pays.xml" << endl;
        cout << "2: Visiter un voisin" <<endl;
        cout << "3: Visiter le pays" << endl;
        cout << "4: Chemin optimal d'une ville à l'autre avec un moyen de transport unique " << endl;
        cout << "5: Chemin optimal d'une ville à l'autre avec tous moyens de transport confondus " << endl;
        cout << "6: Quitter " << endl;
        cin>>ch;
        cin.clear();
        vider_buffer();
        switch (ch)
        {
            case 1:
                facade = new Facade(Prog::chargerPaysXml());
                init=1;
                cout << "SUCCES" <<endl;
                break;
            case 2:
                if (init ==0)
                {
                    cout << "\n Pays non initialise, choisir l'option 1" <<endl;
                    break;
                }
                facade->chercherVoisin();
                break;
            case 3:
                if (init ==0)
                {
                    cout << "\n Pays non initialise, choisir l'option 1" <<endl;
                    break;
                }
                clearscreen();
                facade->afficherGraphe();
                break;
            case 4:
                if (init ==0)
                {
                    cout << "\n Pays non initialise, choisir l'option 1" <<endl;
                    break;
                }
                facade->meilleurChemin();
                break;
            case 5:
                if (init == 0)
                {
                    cout << "\n Pays non initialise, choisir l'option 1" <<endl;
                    break;
                }
                clearscreen();
                facade->meilleurCheminTousTransport();
                break;
            case 6:
                cont=0;
                break;
            default :
                cout << "\n Choix du menu non compris" <<endl;
                break;
        }
         if(cont > 0)
            {
                cout << "Appuyer sur une touche pour continuer" << endl;
                getTouch();
            }
        clearscreen();
    }
}


Pays * Prog ::remplirPaysMan() //graphe interne
{
    Pays * pays;
    //NOM
    string nom ="FRANCE";
    vector < Ville * > villes;
    vector < Gare * > g1;
    vector < Gare * > g2;
    vector < Gare * > g3;
    vector < Gare * > g4;
    g1.push_back(Gare::createObjectMan("Montparnasse",Ferroviaire));
    g1.push_back(Gare::createObjectMan("Austerlitz",Ferroviaire));
    g1.push_back(Gare::createObjectMan("Saint lazare",Ferroviaire));
    g1.push_back(Gare::createObjectMan("Charles de Gaulle",Aeroport));
    g2.push_back(Gare::createObjectMan("Gare du Mans",Ferroviaire));
    g3.push_back(Gare::createObjectMan("Saint Charles",Ferroviaire));
    g4.push_back(Gare::createObjectMan("Part Dieu",Ferroviaire));
    g4.push_back(Gare::createObjectMan("Perrache",Ferroviaire));
    Ville * v1 = Ville::createObjectMan("Paris",g1);
    Ville * v2 = Ville::createObjectMan("Le Mans",g2);
    Ville * v3 = Ville::createObjectMan("Marseille",g3);
    Ville * v4 = Ville::createObjectMan("Lyon",g4);
    villes.push_back(v1);
    villes.push_back(v2);
    villes.push_back(v3);
    villes.push_back(v4);
    vector < vector < vector < Connexion * > > > matrice = Pays::matriceDepart(4);
    // PARIS - Le MANS - TRAIN
    matrice[1][0][1]= new Connexion(209,60,50);
    //  Le MANS PARIS- TRAIN
    matrice[1][1][0]= new Connexion(209,60,60);
    //PARIS MARSEILLE -TRAIN
    matrice[1][0][2]= new Connexion(774,195,100);
    //MARSEILLE PARIS -TRAIN
    matrice[1][2][0]= new Connexion(774,195,120);
    // PARIS - LYON - TRAIN
    matrice[1][0][3]= new Connexion(391,120,80);
    //  LYON - PARIS- TRAIN
    matrice[1][3][0]= new Connexion(391,120,90);
    // PARIS - Le MANS - ROUTE
    matrice[0][0][1]= new Connexion(209,120,40);
    //  Le MANS PARIS- ROUTE
    matrice[0][1][0]= new Connexion(209,120,40);
    pays = Pays::createObjectMan(nom,villes,matrice);
    return pays;

}

Pays * Prog ::remplirPays()
{
    int i;
    int n;
    cout << "\n Nous allons construire un graphe representant les  villes et connexions en France" <<endl;
    cout << "\n Pour cela, saisissez d'abord le nombre de villes" <<endl;
    cin>>n;
    for(i=0; i<n; i++)
    {

    }
    return NULL;
}

Pays * Prog ::chargerPaysXml()
{
    Pays * pays;
    vector < Ville * > villes;
    vector < vector < vector < Connexion * > > > matrice;
    TiXmlDocument doc("pays1.xml");
    if(!doc.LoadFile())
    {
        cerr << "erreur lors du chargement" << endl;
        cerr << "error #" << doc.ErrorId() << " : " << doc.ErrorDesc() << endl;
        exit(1);
    }
    TiXmlHandle hdl(&doc);
    TiXmlElement *elem, *elem2 ;
//Pays
    string nomPays;
    elem=hdl.FirstChildElement("Pays").Element();
    nomPays=elem->Attribute("nom");
//Villes
    elem = hdl.FirstChildElement("Pays").FirstChildElement("ListeVilles").FirstChildElement().Element();
    int i=0;
    while(elem)
    {
        string nom;
        vector <Gare * > gares;
        elem2 = hdl.FirstChildElement("Pays").FirstChildElement("ListeVilles").Child("Ville",i).FirstChildElement().Element();
        while(elem2)
        {
            string nomGare;
            TypeGare type;
            int a;
            nomGare=elem2->Attribute("nom");
            elem2->QueryIntAttribute("type",&a);
            if (a==0)
                type=Ferroviaire;
            else type=Aeroport;
            gares.push_back(Gare::createObjectMan(nomGare,type));
            elem2=elem2->NextSiblingElement();
        }
        nom=elem->Attribute("nom");
        villes.push_back(Ville::createObjectMan(nom,gares));
        elem=elem->NextSiblingElement();
        i++;
    }
    matrice=Pays::matriceDepart(villes.size());
//Connexions
    elem = hdl.FirstChildElement("Pays").FirstChildElement("ListeConnexions").FirstChildElement().Element();
    int i1,i2,i3,cpteur;
    float distance,duree,prix;
    cpteur =1;
    while(elem)
    {
        elem->QueryIntAttribute("mode",&i1);
        elem->QueryIntAttribute("depart",&i2);
        elem->QueryIntAttribute("arrivee",&i3);
        elem->QueryFloatAttribute("distance",&distance);
        elem->QueryFloatAttribute("duree",&duree);
        elem->QueryFloatAttribute("prix",&prix);
        if((i1 >= 0)&&(i2 >= 0)&&(i3 >= 0)&&(i1 < 3)&&(i2< (int) villes.size())&&(i3 < (int) villes.size()))
            matrice[i1][i2][i3]=new Connexion(distance,duree,prix);
        else
            cout << "\n Probleme d'indice avec la connexion numero " << cpteur <<endl;
        elem=elem->NextSiblingElement();
        cpteur++;
    }
    pays=Pays::createObjectMan(nomPays,villes,matrice);
    return pays;
}
/*******/

/**** Façade ***/
Facade::Facade()
{

}



Facade::Facade(Pays * pays)
{
    this->pays = pays;
    this->adapte = new AdaptePays(pays,0);
}
Pays * Facade::getPays()
{
    return this->pays;
}

AdaptePays * Facade::getAdaptePays()
{
    return this->adapte;
}


void Facade::chargerGraphe()
{
    Prog::chargerPaysXml();
}

void Facade::afficherGraphe()
{
    int n;
    string nomVille;
    clearscreen();
    cout << "Sélectionnez le mode d'affichage" << endl << " 1. Exhaustivement" <<endl <<" 2. Par matrice d'adjacence" << endl << " 3. Liste de ville par moyen de transport" << endl;
    cin >> n;
        if(n == 1)
        {
            cout << "Entrez le nom de la ville : "   << endl;
            cin>>nomVille;
            if (n > (int)this->pays->getVilles().size()||n<1)
            {
                cout << "\n Probleme d'indice" <<endl;
            }
            clearscreen();
            this->pays->visite(this->getPays()->getIndiceVille(nomVille));
        }
        else if(n == 2)
        {
             cout << "Sélectionnez le critère de valeur :"   << endl<< "   1.Distance"<< endl<<"   2.Duree"<<endl<<"   3.Prix"<<endl;
             cin>>n;
            if(n < 4 && n > 0)
                this->adapte->changeCritere(this->pays,n-1);
            clearscreen();
            this->adapte->afficherMatrice();
        }
        else if(n==3)
        {
            this->afficherVillesbyTransportation();
        }
}

void Facade::meilleurChemin()
{
    string nom1, nom2, critere;
    while(this->getPays()->getIndiceVille(nom1) == (int) this->getPays()->getVilles().size() || this->getPays()->getIndiceVille(nom2) == (int) this->getPays()->getVilles().size())
    {
        clearscreen();
        cout << "Entrez le nom de la ville de départ  "   << endl;
        cin >>nom1;
        cout << "Entrez le nom de la ville d'arrivée  "   << endl;
        cin >>nom2;
        cout << "Entrez le critère de recherche" << endl; //vpir fonction Facade::meilleurChemin(string ville1, string ville2, string critere)
        cin >> critere;
    if(this->getPays()->getIndiceVille(nom1) == (int) this->getPays()->getVilles().size() || this->getPays()->getIndiceVille(nom2) == (int) this->getPays()->getVilles().size())
        {
            clearscreen();
            cout << "La ou les villes que vous avez rentré n'existent pas. Veuillez rentrer des villes existantes." <<endl <<"Appuyer sur une touche pour continuer"<<endl;
            getTouch();
        }
    }
    clearscreen();
    this->meilleurChemin(nom1,nom2,critere);
}

void Facade::meilleurChemin(string ville1, string ville2, string critere)
{
    string critere1, critere2;
    int c1 = 0, c2 = 2;
    if((int) critere.size() == 1)
    {
        critere2.assign(critere,0,1);
        if(critere2 == "1")
            c2 = 0;
        else if  (critere2 == "2")
            c2 = 1;
        else if(critere2 == "3")
            c2 = 2;
    }else
    {
        if((int) critere.size() > 4)
            critere1.assign(critere, 0, 5);
        if((int) critere.size() > 5)
            critere2.assign(critere,5,1);
        if(critere1 == "route")
            c1 = 0;
        else if  (critere1 == "avion")
            c1 = 2;
        else if(critere1 == "train")
            c1 = 1;
        if(critere2 == "1")
            c2 = 0;
        else if  (critere2 == "2")
            c2 = 1;
        else if(critere2 == "3")
            c2 = 2;
    }
    this->getPays()->meilleurCheminCritere(this->getPays()->getIndiceVille(ville1),this->getPays()->getIndiceVille(ville2),c1,c2);
}

void Facade::meilleurCheminTousTransport()
{
    string nom1, nom2, critere = "";
    while(this->getPays()->getIndiceVille(nom1) == (int) this->getPays()->getVilles().size() || this->getPays()->getIndiceVille(nom2) == (int) this->getPays()->getVilles().size())
    {
        clearscreen();
        cout << "Entrez le nom de la ville de départ  "   << endl;
        cin >>nom1;
        cout << "Entrez le nom de la ville d'arrivée  "   << endl;
        cin >>nom2;
        cout << "Entrez le critère de recherche" << endl; //vpir fonction Facade::meilleurChemin(string ville1, string ville2, string critere)
        cin >> critere;
    if(this->getPays()->getIndiceVille(nom1) == (int) this->getPays()->getVilles().size() || this->getPays()->getIndiceVille(nom2) == (int) this->getPays()->getVilles().size())
        {
            clearscreen();
            cout << "La ou les villes que vous avez rentré n'existent pas. Veuillez rentrer des villes existantes." <<endl <<"Appuyer sur une touche pour continuer"<<endl;
            getTouch();
        }
    }
    clearscreen();
    this->meilleurCheminTousTransport(nom1,nom2,critere);

}

void Facade::meilleurCheminTousTransport(string ville1, string ville2, string critere)
{
    int c = 2;
    if(critere == "prix")
        c = 2;
    else if(critere == "distance")
        c = 0;
    else if(critere == "duree")
        c = 1;
    this->getAdaptePays()->changeCritere(this->getPays(), c-1);
    //this->getAdaptePays()->meilleurCheminCritereToutMode(this->getPays()->getIndiceVille(ville1),this->getPays()->getIndiceVille(ville2),c);
    this->getAdaptePays()->meilleurCheminEscale(this->getPays()->getIndiceVille(ville1),this->getPays()->getIndiceVille(ville2));
}

void Facade::chercherVoisin()
{
    bool ok =true;
    clearscreen();
    string nom;
    int degre;
    cout << "Entrer le nom de la ville d'origine"<<endl;
    cin >> nom;
    if(this->getPays()->getIndiceVille(nom) == (int) this->getPays()->getVilles().size())
    {
        ok=false;
        cin.clear();
        vider_buffer();
        cout<<"Ville inconnue, veuillez recommencer l'opération" <<endl;
        getTouch();
        this->chercherVoisin();
    }
    if(ok==true){
    cout << "Donner le degré d'exploration du voisin"<<endl;
    cin >> degre;
    if(!cin.good())
    {
        cin.clear();
        vider_buffer();
        cout<<"Valeur incomprise, veuillez recommencer l'opréation" <<endl;
        getTouch();
        this->chercherVoisin();
    }
    clearscreen();
    vector <int> villeExplorees;
    this->getPays()->explorer(this->getPays()->getIndiceVille(nom),villeExplorees,degre);
    }
}

void Facade::afficherVillesbyTransportation()
{
    clearscreen();
    string critere;
    cout << "Ville avec gare ou aeroport" << endl;
    cin >> critere;
    clearscreen();
    this->afficherVillesbyTransportation(critere);
}

void Facade::afficherVillesbyTransportation(string critere)
{
    vector<Ville *> villes;
    if(critere == "gare")
        villes = this->getPays()->getVillesFerroviaire();
    else if (critere == "aeroport")
        villes = this->getPays()->getVillesAeroportuaire();
    else
        cout << "Critère incompréhensible" << endl;
    if(critere == "gare" || critere == "aeroport")
    {
        cout << "Liste des villes :" <<endl;
        for(int i = 0; i< (int) villes.size();i++)
            cout << " "<<villes[i]->getNom() << endl;
    }
}

